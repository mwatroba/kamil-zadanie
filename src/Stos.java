/**
 * Created by Kamil
 */
public class Stos<T> {
    private ListaJednokierunkowa<T> lista = new ListaJednokierunkowa<>();

    void dodajNaStos(T wartosc) {
        lista.dodajNaPoczątek(wartosc);
    }

    T pobierzWartoscZeStou() {
        if (lista.rozmiar() == 0) {
            throw new Error("Sots jest pusty");
        }
        return lista.pobierzElement(0);
    }

    void usunPierwszyElementZeStosu() {
        if (lista.rozmiar() == 0) {
            throw new Error("Sots jest pusty");
        }
        lista.usunOstatni();
    }

    public static void main(String[] args) {

    }
}
