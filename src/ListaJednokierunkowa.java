/**
 * Główna klasa Listy jednokierunkowej
 * @param <T>
 */
public class ListaJednokierunkowa<T> {

    /**
     * Element początkowy listy
     * Jeśli lista jest pusta to jest wrtość null
     */
    private Element poczatek;

    /**
     * zwraca rozmiar listy - patrz rozmiarRekurencyjnie(...)
     * @return
     */
    public int rozmiar() {
        return rozmiarRekurencyjnie(poczatek);
    }

    /**
     * Metoda zwraca wielkość listy od danego elementu
     * Tutaj jesyt operator ?:
     * warunek ? jeślitak : jeślinie
     */
    private int rozmiarRekurencyjnie(Element element) {
        return element == null ? 0 : 1 + rozmiarRekurencyjnie(element.nastepny);
    }

    /**
     * Zwraca element z listy, sprawdza czy indeks nie jest ujemny
     */
    public T pobierzElement(int index) {
        if (index < 0) {
            throw new Error("Niepoprawny index");
        } else {
            return pobierzElementRekurencyjnie(poczatek, index);
        }
    }

    /**
     * Sprawdza czy nie wyszło się poza indeks i przybliża się do elementu
     */
    private T pobierzElementRekurencyjnie(Element element, int ilosc) {
        if (element == null) {
            throw new Error("Niepoprawny index");
        } else if (ilosc == 0) {
            return element.wartosc;
        } else {
            return pobierzElementRekurencyjnie(element.nastepny, ilosc - 1);
        }
    }

    /**
     * Dodaje element na początek
     */
    public void dodajNaPoczątek(T wartosc) {
        poczatek = new Element(wartosc, poczatek);
    }

    /**
     * Dodaje element na koniec
     */
    public void dodajNaKoniec(T wartosc) {
        dodajNaIndex(wartosc, rozmiar());
    }

    /**
     * Dodaje element na kolejny indeks - spróbuj zrozumieć kod to zczaisz jak to działa
     * - powiązane z dodajNaIndeksRekurencyjnie
     */
    public void dodajNaIndex(T wartosc, int index) {
        if (index < 0) {
            throw new Error("Niepoprawny index");
        } else if (index == 0) {
            dodajNaPoczątek(wartosc);
        } else dodajNaIndeksRekurencyjnie(poczatek, wartosc, index - 1);
    }

    private void dodajNaIndeksRekurencyjnie(Element element, T wartosc, int index) {
        if (index == 0) {
            Element nastepny = element.nastepny;
            element.nastepny = new Element(wartosc, nastepny);
        } else {
            dodajNaIndeksRekurencyjnie(element.nastepny, wartosc, index - 1);
        }
    }

    /**
     * usuwa pierwszy element z listy
     */
    public void usunPierwszy() {
        if (poczatek == null) {
            throw new Error("Lista jest pusta");
        } else {
            poczatek = poczatek.nastepny;
        }
    }

    /**
     * usuwa element z indeksu, patrz usunZIndexuRekurencyjnie
     */
    public void usunZIndexu(int index) {
        if (index < 0) {
            throw new Error("Niepoprawny index");
        } else if (index == 0) {
            usunPierwszy();
        } else usunZIndexuRekurencyjnie(index - 1, poczatek);
    }

    /**
     * usuwa ostatani element - odpowiednie wykorzystanie usunZIndexu
     */
    public void usunOstatni() {
        usunZIndexu(rozmiar() - 1);
    }

    /**
     * wykozrystywana do usuwania eleemntu z indeksu - patrz usunZIndexu
     * @param index
     * @param element
     */
    private void usunZIndexuRekurencyjnie(int index, Element element) {
        if (element.nastepny == null) {
            throw new Error("Niepoprawny index");
        } else if (index == 0) {
            element.nastepny = element.nastepny.nastepny;
        } else {
            usunZIndexuRekurencyjnie(index - 1, element.nastepny);
        }
    }

    /**
     * zwraca String reprezentujący całą listę - patrz Element.elementString()
     * @return
     */
    public String listString() {
        if (poczatek == null) return "[]";
        else return "[" + poczatek.elementString() + "]";
    }

    /**
     * Klasa wewnętrzna reprezentująca element
     */
    class Element {
        /**
         * wartość elementu
         */
        T wartosc;

        /**
         * Odnośnik do następnego elementu
         */
        Element nastepny;

        /**
         * Konstruktor
         */
        Element(T wartosc, Element nastepny) {
            this.wartosc = wartosc;
            this.nastepny = nastepny;
        }

        /**
         * Zwrca wartośc elementu (jest dodatkowe zabezpieczenie przed błędem, bo z null nie można wywołać toString())
         */
        String wartoscString() {
            return wartosc == null ? "null" : wartosc.toString();
        }

        /**
         * Zwraca String elementów od tego elementu
         * np "0", "0, 1", "0, 1, 2" (potem jest to w metodzie listy listString() pakowane w nawias [])
         */
        String elementString() {
            String nastepnyString = (nastepny == null)
                    ? ""
                    : ", " + nastepny.elementString();
            return wartoscString() + nastepnyString;
        }
    }

    /**
     * Metoda do drukowania w konsoli, żebu nie pisać za każdym razem System.out.println(...)
     * jest statyczna i nie potrzeba instancji klasy, aby działała
     */
    public static void println(String value) {
        System.out.println(value);
    }

    /**
     * Podobnie jak wyżej, ale przechodzi tylko do następnej lini
     */
    public static void println() {
        System.out.println();
    }

    /**
     * Tę metodę uruchamiasz, żeby przetestować działanie listy
     * @param args
     */
    public static void main(String[] args) {
        ListaJednokierunkowa<Integer> lista = new ListaJednokierunkowa<>();
        println("Pusta lista");
        println(lista.listString());

        lista.dodajNaPoczątek(1);
        println();
        println("Dodanie elementu na początek");
        println(lista.listString());

        lista.dodajNaPoczątek(2);
        println();
        println("Dodanie elementu na początek");
        println(lista.listString());

        lista.dodajNaKoniec(3);
        println();
        println("Dodanie elementu na koniec");
        println(lista.listString());

        lista.dodajNaIndex(4, 1);
        println();
        println("Dodanie elementu na index 1");
        println(lista.listString());

        lista.dodajNaIndex(5, 1);
        println();
        println("Dodanie elementu na index 1");
        println(lista.listString());

        lista.dodajNaIndex(6, 1);
        println();
        println("Dodanie elementu na index 1");
        println(lista.listString());

        println();
        println("Pobranie elementu z 2 indexu");
        println(lista.pobierzElement(2).toString());

        println();
        println("Usuniecie elementu z indeksu");
        lista.usunZIndexu(1);
        println(lista.listString());

        println();
        println("Usuniecie ostatniego elementu");
        lista.usunOstatni();
        println(lista.listString());

        println();
        println("Usuniecie pierwszego elementu");
        lista.usunPierwszy();
        println(lista.listString());
    }

}
