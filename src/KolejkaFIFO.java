/**
 * Created by Kamil
 */
public class KolejkaFIFO<T> {
    private ListaJednokierunkowa<T> lista = new ListaJednokierunkowa<>();

    void dodajDokolejki(T wartosc) {
        lista.dodajNaKoniec(wartosc);
    }

    void usunPierwszy() {
        if (lista.rozmiar() == 0) {
            throw new Error("Kolejka jest pusta");
        }
        lista.usunPierwszy();
    }

    T pobierzPierwszyElement() {
        if (lista.rozmiar() == 0) {
            throw new Error("Kolejka jest pusta");
        }
        return lista.pobierzElement(0);
    }

    public static void main(String[] args) {

    }
}
